import axios from 'axios';

const GET_ALL_TAGS_URL = "/tags";

export const loadAllTagsRequest = () => {
    return axios.get(GET_ALL_TAGS_URL)
        .then(response => {
            return response.data
        })
};


