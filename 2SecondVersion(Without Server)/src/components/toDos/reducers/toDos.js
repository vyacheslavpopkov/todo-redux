import * as types from '../actions/ActionTypes';

let initialState = {
    toDos: [
        {
            id: 0,
            title: "I must Learn React Js",
            isDone: false
        },
        {
            id: 1,
            title: "I must write the code in the language React Js",
            isDone: false
        },
        {
            id: 2,
            title: "I must learn Redux",
            isDone: false
        }
    ],
    toDo: {},
    temporaryToDo: "",
    stateEdit: {
        isEdit: false,
        toDo: {}
    }
};

function toDosReducer(state = initialState, action) {
    switch (action.type) {

        case types.ADD_TODO:
            return updateObject(state, {
                toDos: state.toDos.concat(action.toDo),
                temporaryToDo: ""
            });

        case types.UPDATE_TODO:
            return updateObject(state, {
                toDos: state.toDos.map((item) => {
                    if(item.id === action.toDo.id){
                        return action.toDo;
                    } else return item
                }),
                temporaryToDo: "",
                stateEdit: {
                    isEdit: false,
                    toDo: {}
                }
            });


        case types.DELETE_TODO:
            return updateObject(state, {
                toDos: state.toDos.filter(item => item.id !== action.id)
            });

        case types.COMPLETE_TODO:
            return updateObject(state, {
                toDos: state.toDos.map(item => {
                    if (item.id === action.id) {
                        item.isDone = !item.isDone;
                    }
                    return item;
                })
            });

        case types.EDIT_TODO:
            return updateObject(state, {
                temporaryToDo: state.toDos[action.id].title,
                stateEdit: {
                    isEdit: true,
                    toDo: state.toDos[action.id]
                }
            });

        case types.UPDATE_VALUE:
            return updateObject(state, {
                temporaryToDo: action.value
            });


        default:
            return state;
    }

    function updateObject(obj, newProperties) {
        return Object.assign({}, obj, newProperties);
    }
}

export default toDosReducer;
