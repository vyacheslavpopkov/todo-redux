import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createLogger } from 'redux-logger'

import toDosReducer from './components/toDos/reducers/toDos';
import toDoReducer from './components/toDo/reducers/toDo';

const middleware = [
    thunk,
    createLogger(),
];


const store = createStore(
    combineReducers({
        toDosReducer: toDosReducer,
        toDoReducer: toDoReducer
    }),
    composeWithDevTools(applyMiddleware(...middleware))
);

export default store;


