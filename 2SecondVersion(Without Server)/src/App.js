import React, { Component } from 'react';
import ToDosContainer from './components/toDos/ToDosContainer'

class App extends Component {

  render() {
    return (
      <ToDosContainer/>
    );
  }
}

export default App;
