import React, { Component } from "react";
import { connect } from "react-redux";
import ToDo from './ToDo'
import { FormControl, Button } from "react-bootstrap";
import { addToDo } from './actions/toDos';

class ToDosContainer extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            temporaryToDo: ""
        }
    }
    
    updateToDos = (e) => {
        this.setState({
            temporaryToDo:  e.target.value
        });
    }

    addToDo = (e) => {
        let toDo = {
            id: this.props.toDos.length + 1,
            title: this.state.temporaryToDo
        }
        this.props.addToDo(toDo);
        this.setState({ temporaryToDo:  "" });
    }

    render() {
        return (
            <div>
                {
                    this.props.toDos.map((item, key) =>
                        <ToDo key={item.id} title={item.title} />)
                }
                <FormControl
                    value={this.state.temporaryToDo}
                    onChange={this.updateToDos.bind(this)} />

                <Button onClick={this.addToDo.bind(this)}>Save</Button>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        toDos: state.toDosReducer.toDos,
        toDo: state.toDosReducer.toDo
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        addToDo: (toDo) => dispatch(addToDo(toDo))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ToDosContainer);
