import * as types from '../actions/ActionTypes';

let initialState = {
    toDos:  [
        {
            id: 0,
            title: "I must Learn React Js"
        },
        {
            id: 1,
            title: "I must write the code in the language React Js"
        },
        {
            id: 2,
            title: "I must learn Redux"
        }
    ],
    toDo: {}
};

function toDosReducer(state = initialState, action) {
    switch (action.type) {

        case types.ADD_TODO:
            return updateObject(state, {
                toDos: state.toDos.concat(action.toDo)
            });

        default:
            return state;
    }

    function updateObject(obj, newProperties) {
        return Object.assign({}, obj, newProperties);
    }
}

export default toDosReducer;
