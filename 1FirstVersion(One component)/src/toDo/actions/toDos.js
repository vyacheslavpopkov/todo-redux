import * as types from './ActionTypes';

export const addToDo = (toDo) => {
    return {
        type: types.ADD_TODO,
        toDo
    }
}