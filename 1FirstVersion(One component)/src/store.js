import { combineReducers, createStore } from 'redux';

import toDosReducer from './toDo/reducers/toDos';

const store = createStore(
    combineReducers({
        toDosReducer: toDosReducer
    })
);

export default store; 
