import React, { Component } from 'react';
import ToDosContainer from './toDo/ToDosContainer'

class App extends Component {

  render() {
    return (
      <ToDosContainer/>
    );
  }
}

export default App;
