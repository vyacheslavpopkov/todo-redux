import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createLogger } from 'redux-logger';
import { routerReducer } from 'react-router-redux'

import authorizationReducer from './components/authorization/reducers/authorization';
import headerReducer from './components/header/reducers/header';
import toDoReducer from './components/toDo/reducers/toDo';
import toDosReducer from './components/toDos/reducers/toDos';
import aboutReducer from './components/about/reducers/about';

import authorizationMiddleware from './components/authorization/middleware/authorization';
import localizationMiddleware from './components/localization/middleware/localization';
import toDoMiddleware from './components/toDo/middleware/toDo';
import toDosMiddleware from './components/toDos/middleware/toDos'

const middleware = [
    thunk,
    authorizationMiddleware,
    localizationMiddleware,
    toDoMiddleware,
    toDosMiddleware,
    createLogger(),
];


const store = createStore(
    combineReducers({
        authorizationReducer: authorizationReducer,
        headerReducer: headerReducer,
        toDoReducer: toDoReducer,
        toDosReducer: toDosReducer,
        aboutReducer: aboutReducer,
        routing: routerReducer
    }),
    composeWithDevTools(applyMiddleware(...middleware))
);

export default store;


