import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from 'react-router';
import { Route, Switch } from 'react-router-dom';
import { FormControl, Button, Grid } from "react-bootstrap";
import { loadLocalization, updateMail, updatePassword, logIn } from './actions/authorization';
import HeaderContainer from '../header/HeaderContainer';
import AboutContainer from '../about/AboutContainer';
import ToDosContainer from '../toDos/ToDosContainer';
import { getLocalization } from '../localization/middleware/localization';
import { getLocalText } from '../localization/LocalizationContainer';
import './css/authorization.css';


class Main extends React.Component {
    render() {
        return <main>
            <Switch>
                <Route exact path='/todos' component={ToDosContainer} />
                <Route path='/about' component={AboutContainer} />
            </Switch>
        </main>
    }
}

class AuthorizationContainer extends Component {

    componentWillMount(){
        this.props.loadLocalization();
    }

    updateMail = (e) => {
        this.props.updateMail(e.target.value);
    }

    updatePassword = (e) => {
        this.props.updatePassword(e.target.value);
    }

    logIn = () => {
        let user = {
            login: this.props.mail,
            password: this.props.password
        }
        this.props.logIn(user);
    }

    render() {
        return (
            (this.props.localDataWasObtained)
            ?(<div>
                {
                    (this.props.isInside)
                        ? <div>
                            <HeaderContainer />
                            <Main />
                        </div>
                        : <Grid>
                            <FormControl
                                value={this.props.mail}
                                onChange={this.updateMail.bind(this)} />
                            <FormControl
                                value={this.props.password}
                                onChange={this.updatePassword.bind(this)} />

                            <Button onClick={this.logIn.bind(this)}>logIn</Button>
                            {
                                (this.props.error.isError)
                                    ? <div id="error"> {this.props.error.errorText} </div>
                                    : <div> </div>
                            }
                        </Grid>
                }
            </div>)
            :<div></div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        localDataWasObtained: state.authorizationReducer.localDataWasObtained,
        mail: state.authorizationReducer.mail,
        password: state.authorizationReducer.password,
        isInside: state.authorizationReducer.isInside,
        error: state.authorizationReducer.error
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        loadLocalization: () => dispatch(loadLocalization()),
        updateMail: (mail) => dispatch(updateMail(mail)),
        updatePassword: (password) => dispatch(updatePassword(password)),
        logIn: (user) => dispatch(logIn(user))
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AuthorizationContainer));
