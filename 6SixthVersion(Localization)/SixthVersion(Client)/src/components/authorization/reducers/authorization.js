import * as types from '../actions/ActionTypes';

let initialState = {
    localDataWasObtained: false,
    mail: "",
    password: "",
    isInside: false,
    error: {
        errorText: "",
        isError: false
    },
    token: "",
    role: ""
};

function authorizationReducer(state = initialState, action) {
    switch (action.type) {

        case types.LOCALIZATION_WAS_SUCCESSFUL:
            return updateObject(state, {
                localDataWasObtained: true
            });

        case types.UPDATE_MAIL:
            return updateObject(state, {
                mail: action.mail,
                error: {
                    errorText: "",
                    isError: false
                }
            });

        case types.UPDATE_PASSWORD:
            return updateObject(state, {
                password: action.password,
                error: {
                    errorText: "",
                    isError: false
                }
            });

        case types.LOGIN_SUCCESSFUL:
            return updateObject(state, {
                isInside: true,
                token: action.token,
                role: action.role
            });

        case types.LOGOUT:
            return updateObject(state, {
                isInside: false,
                mail: "",
                password: "",
                token: "",
                role: ""
            });

        case types.LOGIN_ERROR:
            return updateObject(state, {
                error: {
                    errorText: "Invalid username or password.",
                    isError: true
                }
            });

        default:
            return state;
    }

    function updateObject(obj, newProperties) {
        return Object.assign({}, obj, newProperties);
    }
}

export default authorizationReducer;
