import React, { Component } from "react";
import { connect } from "react-redux";
import { } from './actions/about';


class AboutContainer extends Component {

    render() {
        return (
            <div>
                <div> {this.props.text} </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        text: state.aboutReducer.text
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AboutContainer);
