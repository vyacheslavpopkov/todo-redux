import * as types from './ActionTypes';

export const changeLanguage = (language, id) => {
    return {
        type: types.CHANGE_LANGUAGE, 
        language, 
        id
    }
}