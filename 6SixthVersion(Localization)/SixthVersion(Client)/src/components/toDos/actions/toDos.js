import * as types from './ActionTypes';

export const addToDo = (toDo) => {
    return {
        type: types.ADD_TODO,
        toDo
    }
}

export const updateValue = (value) => {
    return {
        type: types.UPDATE_VALUE,
        value
    }
}

export const updateToDo = (toDo) => {
    return {
        type: types.UPDATE_TODO,
        toDo
    }
}

export const loadAllToDos = () => {
    return {
        type: types.LOAD_TODOS
    }
}

export const toDosLoaded = (toDos) => {
    return {
        type: types.TODOS_LOADED,
        toDos
    }
}

export const logOut = () => {
    return {
        type: types.LOGOUT
    }
}




