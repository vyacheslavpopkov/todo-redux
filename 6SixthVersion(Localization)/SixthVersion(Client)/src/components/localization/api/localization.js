import axios from '../../globalApi';

const GET_ALL_LOCALIZATION_URL = "/localization/text/";

export const loadAllLocalRequest = () => {
    return axios.get(GET_ALL_LOCALIZATION_URL)
        .then(response => {
            return response.data
        })
};