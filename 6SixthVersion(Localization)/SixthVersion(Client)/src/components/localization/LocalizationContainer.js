import IntlMessageFormat from 'intl-messageformat';
import { getLocalization } from './middleware/localization';

export const getLocalText = (message, language) => {
  var localText = new IntlMessageFormat(message, language);
  var output = localText.format({ numPhotos: 1000 });
  return Object.assign({}, {output});
}
