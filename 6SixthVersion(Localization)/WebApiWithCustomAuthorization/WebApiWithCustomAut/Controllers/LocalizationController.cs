﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApiWithCustomAut.Models;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;


namespace WebApiWithCustomAut.Controllers
{
	[Route("api/localization")]
	public class LocalizationController : Controller
	{		
		[Route("text")]
		[HttpGet]
		public async Task GetLocalization()
		{
			var enUS = new
			{
				NUM_PHOTOS = "You have {numPhotos, plural, =0 {no photos.} =1 {one photo.} other {# photos.}}",
				TEXT1 = "You have {value, plural, =A {I am A.} =B {I am B} other {# photos.}}",
				TEXT = "Hi Alex",
				TODOS = "Todos",
				ABOUT = "About"
			};

			var esMX = new
			{
				NUM_PHOTOS = "Usted {numPhotos, plural, =0 {no tiene fotos.} =1 {tiene una foto.} other {tiene # fotos.}}",
				TEXT1 = "You have {value, plural, =A {I am A.} =B {I am B} other {# photos.}}",
				TEXT = "Hi Alex",
				TODOS = "Que hacer",
				ABOUT = "Acerca de"
			};

			var localList = (new[] { enUS }).ToList();
			localList.Add(esMX);
						
			var response = new
			{
				localList
			};

			// сериализация ответа
			Response.ContentType = "application/json";
			await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
		}
	}
}
