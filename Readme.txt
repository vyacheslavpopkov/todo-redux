1) The project is implemented with the help of one component
2) The project is implemented with the help of two connected components (with help Redux)
3) The project consists of a server and a client. 
   Which are connected with each other with the help of middleware. All operations occur on the server.
4) The project consists of a server and a client. Authentication works on the server and on the client. 
   At authorization we receive a token and with each request a put token in the header of the request.
   Responsibility is divided by roles. Only the administrator can delete.
5) Added Route
6) With localization, local data on the server
7) Added test (action, reducer, snapshot) - for start npn test

For start 
1) Start server
2) npm install - on client
3) npm start - on client
4) globalApi.js - on client has localhost server (doesn't forget change localhost)
5) May be necessary to install libraries "npm install <libraries>"