﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WebApiService.Models;


namespace WebApiService.Controllers
{
    [Route("api/[controller]")]
    public class TodoController : Controller
    {
		private readonly TodoContext _context;

		public TodoController(TodoContext context)
		{
			_context = context;

			if (_context.ToDo.Count() == 0)
			{
				_context.ToDo.Add(new ToDo { Title = "Item1" });
				_context.SaveChanges();
			}
		}

		[Route("todos")]
		[HttpGet]
		public IEnumerable<ToDo> GetAll()
		{
			return _context.ToDo.ToList();
		}
		
		[Route("getById/{id}")]
		[HttpGet("{id}")]
		public IActionResult GetById(int id)
		{
			var item = _context.ToDo.FirstOrDefault(t => t.Id == id);
			if (item == null)
			{
				return NotFound();
			}
			return new ObjectResult(item);
		}

		[Route("update/{item}")]
		[HttpPut("{item}")]
		public IActionResult PutUpdate([FromBody] ToDo item)
		{			
			var todo = _context.ToDo.FirstOrDefault(t => t.Id == item.Id);
			if (todo == null)
			{
				return NotFound();
			}

			todo.IsDone = item.IsDone;
			todo.Title = item.Title;

			_context.ToDo.Update(todo);
			_context.SaveChanges();
			return new NoContentResult();
		}
		
		[Route("complete/{id}")]
		[HttpPut("complete/{id}")]
		public IActionResult PutComplete(int id)
		{
			var todo = _context.ToDo.FirstOrDefault(t => t.Id == id);
			if (todo == null)
			{
				return NotFound();
			}

			todo.IsDone = !todo.IsDone;

			_context.ToDo.Update(todo);
			_context.SaveChanges();
			return new NoContentResult();
		}
		
		[Route("todos")]
		[HttpPost]
		public void Post([FromBody]ToDo value)
		{
			_context.ToDo.Add(new ToDo { Title = value.Title, IsDone = value.IsDone });
			_context.SaveChanges();
		}

		[Route("delete/{id}")]
		[HttpDelete("{id}")]
		public IActionResult Delete(int id)
		{
			var todo = _context.ToDo.FirstOrDefault(t => t.Id == id);
			if (todo == null)
			{
				return NotFound();
			}

			_context.ToDo.Remove(todo);
			_context.SaveChanges();
			return new NoContentResult();
		}
	}
}
