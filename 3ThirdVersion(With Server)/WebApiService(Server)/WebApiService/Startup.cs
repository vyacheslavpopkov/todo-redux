﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using WebApiService.Models;
using Microsoft.EntityFrameworkCore;

namespace WebApiService
{
    public class Startup
    {
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddDbContext<TodoContext>(opt => opt.UseInMemoryDatabase("TodoList"));
			services.AddCors();
			services.AddMvc();
		}

		public void Configure(IApplicationBuilder app)
		{
			app.UseCors(builder => 
				builder
					.AllowAnyOrigin()
					.AllowAnyHeader()
					.AllowAnyMethod());

			app.UseMvc();
		}
		/*
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
			services.AddDbContext<TodoContext>(opt => opt.UseInMemoryDatabase("TodoList"));
			services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
		*/
	}
}
