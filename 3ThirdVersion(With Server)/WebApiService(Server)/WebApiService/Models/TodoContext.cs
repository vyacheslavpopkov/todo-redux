﻿using Microsoft.EntityFrameworkCore;

namespace WebApiService.Models
{
    public class TodoContext : DbContext
	{
		public TodoContext(DbContextOptions<TodoContext> options)
			: base(options)
		{
		}

		public DbSet<ToDo> ToDo { get; set; }
	}
}
