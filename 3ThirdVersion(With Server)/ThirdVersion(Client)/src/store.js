import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createLogger } from 'redux-logger'

import toDosReducer from './components/toDos/reducers/toDos';
import toDoReducer from './components/toDo/reducers/toDo';

import toDoMiddleware from './components/toDo/middleware/toDo';
import toDosMiddleware from './components/toDos/middleware/toDos'

const middleware = [
    thunk,
    toDoMiddleware,
    toDosMiddleware,
    createLogger(),
];


const store = createStore(
    combineReducers({
        toDoReducer: toDoReducer,
        toDosReducer: toDosReducer
        
    }),
    composeWithDevTools(applyMiddleware(...middleware))
);

export default store;


