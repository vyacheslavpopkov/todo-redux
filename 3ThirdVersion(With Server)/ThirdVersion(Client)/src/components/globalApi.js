import axiosСreator from 'axios';

const axios = axiosСreator.create({
  baseURL: 'http://localhost:56780/api',
  //timeout: 1000,
  /*
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Allow': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
    'Access-Control-Allow-Origin': '*',
    "Access-Control-Expose-Headers": "Access-Control-*",
    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
    "Access-Control-Allow-Headers": "Access-Control-*, Origin, X-Requested-With, Content-Type, Accept"
  },
  */
  //headers: { 'crossDomain': true, 'Access-Control-Allow-Origin': '*' },
  //withCredentials: false,
  crossDomain: true,

});

export default axios;

/*
export const getRequest = (url) => {
  return fetch(url, requestOptions('GET'))
    .then(status)
    .then(json)
};

export const putRequest = (url, obj) => {
  return fetch(url, requestOptions('PUT', obj))
    .then(status)
    .then(json)
};

export const postRequest = (url, obj) => {
  return fetch(url, requestOptions('POST', obj))
    .then(status)
    .then(json)
};

export const deleteRequest = (url) => {
  return fetch(url, requestOptions('DELETE'))
    .then(status)
    .then(json)
};

const json = response => response.json();
const status = response => {
  return (response.status >= 200 && response.status < 300)
    ? Promise.resolve(response)
    : Promise.reject(new Error(response.statusText))
}
*/

// const requestOptions = (putMethod, obj) => {
//   const option = {
//     method: putMethod,
//     credentials: "same-origin",
//     headers: {
//       'Accept': 'application/json, text/plain, */*',
//       'Content-Type': 'application/json'
//     }
//   }

//   if (obj) {
//     option.body = JSON.stringify(obj);
//   }

//   return option;
// }

