import React, { Component } from "react";
import { connect } from "react-redux";
import { FormControl, Button, Grid } from "react-bootstrap";
import ToDoContainer from '../toDo/ToDoContainer';
import { addToDo, updateToDo, updateValue, loadAllToDos } from './actions/toDos';

class ToDosContainer extends Component {

    componentDidMount() {
        this.props.loadAllToDos();
    }

    updateValue = (e) => {
        this.props.updateValue(e.target.value);
    }

    addToDo = (e) => {
        let toDo = {
            title: this.props.temporaryToDo,
            isDone: false
        }
        this.props.addToDo(toDo);
    }

    updateToDo = (e) => {
        let toDo = {
            id: this.props.stateEdit.toDo.id,
            title: this.props.temporaryToDo,
            isDone: this.props.stateEdit.toDo.isDone
        }
        this.props.updateToDo(toDo);
    }
    
    render() {
        return (
            <Grid>
                {
                    this.props.toDos.map((item, key) =>
                        <ToDoContainer
                            key={item.id}
                            toDo={item}
                            idDone={item.isDone} />)
                }
                <FormControl
                    value={this.props.temporaryToDo}
                    onChange={this.updateValue.bind(this)} />
                {
                    (this.props.stateEdit.isEdit)
                        ? <Button onClick={this.updateToDo.bind(this)}>Save</Button>
                        : <Button onClick={this.addToDo.bind(this)}>Save</Button>
                }
            </Grid>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        toDos: state.toDosReducer.toDos,
        temporaryToDo: state.toDosReducer.temporaryToDo,
        stateEdit: state.toDosReducer.stateEdit
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        addToDo: (toDo) => dispatch(addToDo(toDo)),
        updateValue: (value) => dispatch(updateValue(value)),
        updateToDo: (toDo) => dispatch(updateToDo(toDo)),
        loadAllToDos: () => dispatch(loadAllToDos())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ToDosContainer);
