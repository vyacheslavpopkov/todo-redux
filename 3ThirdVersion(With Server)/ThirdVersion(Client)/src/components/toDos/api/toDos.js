import axios from '../../globalApi';

const GET_ALL_TODOS_URL = "/todo/todos";


export const loadAllToDosRequest = () => {
    return axios.get(GET_ALL_TODOS_URL)
        .then(response => {
            return response.data
        })
};



