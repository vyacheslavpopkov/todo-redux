import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Col, Button } from "react-bootstrap";
import { deleteToDo, completeToDo, editToDo } from './actions/toDo';
import './css/toDo.css';

class ToDoContainer extends Component {
    
    deleteToDo = () => {
        this.props.deleteToDo(this.props.toDo.id);
    }

    completeToDo = () => {
        this.props.completeToDo(this.props.toDo.id);
    }

    editToDo = () => {
        this.props.editToDo(this.props.toDo);
    }

    render() {
        return (
                <Row>
                    <Col >
                        {
                            (this.props.idDone)
                                ? <div id="doneTask">{this.props.toDo.title} </div>
                                : <div>{this.props.toDo.title} </div> 
                        }
                    </Col>
                    <Col >
                        <Button onClick={this.deleteToDo.bind(this)}>Delete</Button>
                        <Button onClick={this.completeToDo.bind(this)}>Complete</Button>
                        <Button onClick={this.editToDo.bind(this)}>Edit</Button>
                    </Col>
                    <hr/>
                </Row>
        )
    }
}

const mapStateToProps = (state) => {
    return {
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        deleteToDo: (id) => dispatch(deleteToDo(id)),
        completeToDo: (id) => dispatch(completeToDo(id)),
        editToDo: (toDo) => dispatch(editToDo(toDo))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ToDoContainer);
