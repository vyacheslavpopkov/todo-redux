export  const ADD_TODO = 'ADD_TODO';
export  const EDIT_TODO = 'EDIT_TODO';
export  const UPDATE_TODO = 'UPDATE_TODO';
export  const UPDATE_VALUE = 'UPDATE_VALUE';
export  const LOAD_TODOS = 'LOAD_TODOS';
export  const TODOS_LOADED = 'TODOS_LOADED';
export  const RELOAD_ALL_TODOS = 'RELOAD_ALL_TODOS';
export  const LOGOUT = 'LOGOUT';




