import * as types from '../actions/ActionTypes';

let initialState = {
};

function toDoReducer(state = initialState, action) {
    switch (action.type) {

        
        default:
            return state;
    }

    function updateObject(obj, newProperties) {
        return Object.assign({}, obj, newProperties);
    }
}

export default toDoReducer;
