import * as types from '../actions/ActionTypes';
import { loginSuccessful, loginError } from '../actions/authorization';
import { logInRequest, logOutRequest } from '../api/authorization';

function authorizationMiddleware({ getState }) {
    return next => action => {
        switch (action.type) {

            case types.LOGIN:
                logInRequest(action.user)
                    .then(response => {
                        if (response) {
                            next(loginSuccessful(response.access_token, response.role))
                        }
                        else
                            next(loginError())
                    })
                    .catch(e => console.error(e))
                break;

            case types.LOGOUT:
                logOutRequest()
                break;


            default:
                break;
        }
        return next(action)
    }
}


export default authorizationMiddleware;