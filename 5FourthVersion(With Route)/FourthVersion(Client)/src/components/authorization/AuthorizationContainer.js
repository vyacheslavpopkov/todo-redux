import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from 'react-router';
import { NavLink, Route, Switch } from 'react-router-dom';
import { FormControl, Button, Grid } from "react-bootstrap";
import { updateMail, updatePassword, logIn } from './actions/authorization';
import AboutContainer from '../about/AboutContainer';
import ToDosContainer from '../toDos/ToDosContainer';
import './css/authorization.css';


class Main extends React.Component {
    render() {
        return <main>
            <Switch>
                <Route exact path='/todos' component={ToDosContainer} />
                <Route path='/about' component={AboutContainer} />
            </Switch>
        </main>
    }
}

class Header extends React.Component {
    render() {
        return <nav>
            <ul>
                <li><NavLink to='/todos'>ToDos</NavLink></li>
                <li><NavLink to='/about'>About</NavLink></li>
            </ul>
        </nav>
    }
}

class AuthorizationContainer extends Component {

    updateMail = (e) => {
        this.props.updateMail(e.target.value);
    }

    updatePassword = (e) => {
        this.props.updatePassword(e.target.value);
    }

    logIn = () => {
        let user = {
            login: this.props.mail,
            password: this.props.password
        }
        this.props.logIn(user);
    }

    render() {
        return (
            (this.props.isInside)
                ? <div>
                    <Header />
                    <Main />
                </div>
                : <Grid>
                    <FormControl
                        value={this.props.mail}
                        onChange={this.updateMail.bind(this)} />
                    <FormControl
                        value={this.props.password}
                        onChange={this.updatePassword.bind(this)} />

                    <Button onClick={this.logIn.bind(this)}>logIn</Button>
                    {
                        (this.props.error.isError)
                            ? <div id="error"> {this.props.error.errorText} </div>
                            : <div> </div>
                    }
                </Grid>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        mail: state.authorizationReducer.mail,
        password: state.authorizationReducer.password,
        isInside: state.authorizationReducer.isInside,
        error: state.authorizationReducer.error
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateMail: (mail) => dispatch(updateMail(mail)),
        updatePassword: (password) => dispatch(updatePassword(password)),
        logIn: (user) => dispatch(logIn(user))
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AuthorizationContainer));
