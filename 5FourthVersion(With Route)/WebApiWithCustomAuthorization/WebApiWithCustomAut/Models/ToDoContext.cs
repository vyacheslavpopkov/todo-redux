﻿using Microsoft.EntityFrameworkCore;

namespace WebApiWithCustomAut.Models
{
    public class ToDoContext : DbContext
	{
		public ToDoContext(DbContextOptions<ToDoContext> options)
			: base(options)
		{
		}

		public DbSet<ToDo> ToDo { get; set; }
	}
}
