import React from 'react'
import { Provider } from 'react-redux';
import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunk from 'redux-thunk';
import * as Enzyme from "enzyme";
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter() });
import ConnectedToDosContainer from '../ToDosContainer';
import ToDosContainer from '../ToDosContainer';
import ToDoContainer from '../../toDo/ToDoContainer';
import toDosMiddleware from '../middleware/toDos';
import toDosReducer from '../reducers/toDos';

describe('<ToDosContainer />', () => {

    let component, store, middleware;

    beforeEach(() => {
        let middlewares = [
            thunk,
            toDosMiddleware,
        ];
        let testReducers = combineReducers({
            toDosReducer
        });

        store = createStore(testReducers, applyMiddleware(...middlewares));
        component = Enzyme.mount(
            <Provider store={store}>
                <ConnectedToDosContainer />
            </Provider>
        );
    });

    it('renders 1 <ConnectedToDosContainer />', () => {
        expect(component).toHaveLength(1);
    });

    it('ConnectedToDosContainer has 1 ToDosContainer', () => {
        let toDos_Container = component.find(ToDosContainer);
        expect(toDos_Container).toHaveLength(1);
    });

    it('ToDosContainer has 0 ToDoContainer ', () => {
        let toDos_Container = component.find(ToDosContainer);
        let toDoList = toDos_Container.find(ToDoContainer);
        expect(toDoList).toHaveLength(0);
    });
});


