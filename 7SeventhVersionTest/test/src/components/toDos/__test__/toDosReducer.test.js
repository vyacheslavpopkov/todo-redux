import toDosReducer from '../reducers/toDos';
import * as types from '../actions/ActionTypes';

describe('Request to toDos Reducer', () => {

    it('has a default state', () => {
        expect(toDosReducer(undefined, { type: "unexpected" })).toEqual({
            toDos: [],
            toDo: {},
            temporaryToDo: "",
            stateEdit: {
                isEdit: false,
                toDo: {}
            }
        });
    });

    it('can handle ADD_TODO', () => {

        let action_ADD_TODO = {
            type: types.ADD_TODO,
        };

        expect(toDosReducer(undefined, action_ADD_TODO)).toEqual({
            toDos: [],
            toDo: {},
            temporaryToDo: "",
            stateEdit: {
                isEdit: false,
                toDo: {}
            }
        });
    });

    it('can handle EDIT_TODO', () => {

        let action_EDIT_TODO = {
            type: types.EDIT_TODO,
            toDo: {
                id: 4,
                isDone: false,
                title: "AAA"
            }
        };

        expect(toDosReducer(undefined, action_EDIT_TODO)).toEqual({
            toDos: [],
            toDo: {},
            temporaryToDo: "AAA",
            stateEdit: {
                isEdit: true,
                toDo: {
                    id: 4,
                    isDone: false,
                    title: "AAA"
                }
            }
        });

    });


    it('can handle UPDATE_TODO', () => {

        let action_UPDATE_TODO = {
            type: types.EDIT_TODO,
            toDo: {
                id: 4,
                isDone: false,
                title: "BBB"
            }
        };

        expect(toDosReducer(undefined, action_UPDATE_TODO)).toEqual({
            toDos: [],
            toDo: {},
            temporaryToDo: "BBB",
            stateEdit: {
                isEdit: true,
                toDo: {
                    id: 4,
                    isDone: false,
                    title: "BBB"
                }
            }
        });
    });


    it('can handle TODOS_LOADED', () => {
 
        let action_TODOS_LOADED = {
            type: types.TODOS_LOADED,
            toDos: [
                {
                    id: 0,
                    isDone: false,
                    title: "AAA"
                },
                {
                    id: 1,
                    isDone: false,
                    title: "BBB"
                },
                {
                    id: 2,
                    isDone: false,
                    title: "CCC"
                },
                {
                    id: 3,
                    isDone: false,
                    title: "DDD"
                }
            ]
        };

        expect(toDosReducer(undefined, action_TODOS_LOADED)).toEqual({
            toDos: [
                {
                    id: 0,
                    isDone: false,
                    title: "AAA"
                },
                {
                    id: 1,
                    isDone: false,
                    title: "BBB"
                },
                {
                    id: 2,
                    isDone: false,
                    title: "CCC"
                },
                {
                    id: 3,
                    isDone: false,
                    title: "DDD"
                }
            ],
            toDo: {},
            temporaryToDo: "",
            stateEdit: {
                isEdit: false,
                toDo: {}
            }
        });
    });

    it('can handle UPDATE_VALUE', () => {
        
        let action_UPDATE_VALUE = {
            type: types.UPDATE_VALUE,
            value: "BBB"
        };

        expect(toDosReducer(undefined, action_UPDATE_VALUE)).toEqual({
            toDos: [],
            toDo: {},
            temporaryToDo: "BBB",
            stateEdit: {
                isEdit: false,
                toDo: {}
            }
        });
    });
});
