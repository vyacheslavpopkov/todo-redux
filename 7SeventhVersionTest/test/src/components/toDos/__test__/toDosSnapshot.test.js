import React from 'react';
import { Provider } from 'react-redux';
import ToDosContainer from '../ToDosContainer';
import renderer from 'react-test-renderer';
import thunk from 'redux-thunk';
import toDosReducer from '../reducers/toDos';
import { applyMiddleware, combineReducers, createStore } from 'redux';
import toDosMiddleware from '../middleware/toDos'

it('renders a snapshot', () => {

    let middlewares = [
        thunk,
        toDosMiddleware,
    ];
    let testReducers = combineReducers({
        toDosReducer
    });
    let store = createStore(testReducers, applyMiddleware(...middlewares));

    const tree = renderer.create(
        <Provider store={store}>
            <ToDosContainer />
        </Provider>
    ).toJSON();
    expect(tree).toMatchSnapshot();
});