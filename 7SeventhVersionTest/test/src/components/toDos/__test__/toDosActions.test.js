import * as types from '../actions/ActionTypes';
import * as myActions from '../actions/toDos';

describe('AddToDoAction', () => {
    it('should create an action to add toDo', () => {
        const toDo = {
            id: 0,
            isDone: false,
            title: "AAA"
        }
        const exceptedActions = {
            type: types.ADD_TODO,
            toDo
        }
        expect(myActions.addToDo(toDo)).toEqual(exceptedActions)
    })
})

describe('UpdateValueAction', () => {
    it('should create an action to update value', () => {
        const value = "AAA";
        const exceptedActions = {
            type: types.UPDATE_VALUE,
            value
        }
        expect(myActions.updateValue(value)).toEqual(exceptedActions)
    })
})

describe('UpdateToDoAction', () => {
    it('should create an action to update toDo', () => {
        const toDo = {
            id: 0,
            isDone: false,
            title: "AAA"
        }
        const exceptedActions = {
            type: types.UPDATE_TODO,
            toDo
        }
        expect(myActions.updateToDo(toDo)).toEqual(exceptedActions)
    })
})

describe('LoadAllToDosAction', () => {
    it('should create an action to load all todos', () => {
        const exceptedActions = {
            type: types.LOAD_TODOS
        }
        expect(myActions.loadAllToDos()).toEqual(exceptedActions)
    })
})

describe('ToDosLoadedAction', () => {
    it('should create an action to final loaded toDo', () => {
        const toDos = [
            {
                id: 0,
                isDone: false,
                title: "AAA"
            },
            {
                id: 1,
                isDone: false,
                title: "BBB"
            },
            {
                id: 2,
                isDone: false,
                title: "CCC"
            },
            {
                id: 3,
                isDone: false,
                title: "DDD"
            }
        ]
        const exceptedActions = {
            type: types.TODOS_LOADED,
            toDos
        }
        expect(myActions.toDosLoaded(toDos)).toEqual(exceptedActions)
    })
})

describe('LogOutAction', () => {
    it('should create an action to logOut', () => {

        const exceptedActions = {
            type: types.LOGOUT
        }
        expect(myActions.logOut()).toEqual(exceptedActions)
    })
})
