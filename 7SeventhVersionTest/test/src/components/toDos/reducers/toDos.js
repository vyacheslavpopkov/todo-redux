import * as types from '../actions/ActionTypes';

let initialState = {
    toDos: [],
    toDo: {},
    temporaryToDo: "",
    stateEdit: {
        isEdit: false,
        toDo: {}
    }
};

function toDosReducer(state = initialState, action) {
    if (action) {
        switch (action.type) {

            case types.TODOS_LOADED:
                return updateObject(state, {
                    toDos: action.toDos
                });


            case types.ADD_TODO:
                return updateObject(state, {
                    temporaryToDo: ""
                });

            case types.EDIT_TODO:
                return updateObject(state, {
                    temporaryToDo: action.toDo.title,
                    stateEdit: {
                        isEdit: true,
                        toDo: action.toDo,
                    }
                });

            case types.UPDATE_TODO:
                return updateObject(state, {
                    temporaryToDo: "",
                    stateEdit: {
                        isEdit: false,
                        toDo: {},
                    }
                });

            case types.UPDATE_VALUE:
                return updateObject(state, {
                    temporaryToDo: action.value
                });

            default:
                return state;
        }
    }

    function updateObject(obj, newProperties) {
        return Object.assign({}, obj, newProperties);
    }
}

export default toDosReducer;
