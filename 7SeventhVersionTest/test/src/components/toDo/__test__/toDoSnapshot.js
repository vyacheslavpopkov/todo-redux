import React from 'react';
import { Provider } from 'react-redux';
import ToDoContainer from '../ToDoContainer';
import renderer from 'react-test-renderer';
import thunk from 'redux-thunk';
import toDoReducer from '../reducers/toDo';
import authorizationReducer from '../../authorization/reducers/authorization';
import { applyMiddleware, combineReducers, createStore } from 'redux';
import toDoMiddleware from '../middleware/toDo'

it('renders a snapshot', () => {
    
    let middlewares = [
        thunk,
        toDoMiddleware,
    ];
    let testReducers = combineReducers({
        toDoReducer,
        authorizationReducer
    });
    let store = createStore(testReducers, applyMiddleware(...middlewares));

    const tree = renderer.create(
        <Provider store={store}>
            <ToDoContainer />
        </Provider>
    ).toJSON();
    expect(tree).toMatchSnapshot();
});