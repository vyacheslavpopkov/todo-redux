import * as types from './ActionTypes';

export const loadLocalization = () => {
    return {
        type: types.LOAD_LOCALIZATION
    }
}

export const updateMail = (mail) => {
    return {
        type: types.UPDATE_MAIL,
        mail
    }
}

export const updatePassword = (password) => {
    return {
        type: types.UPDATE_PASSWORD,
        password
    }
}

export const logIn = (user) => {
    return {
        type: types.LOGIN,
        user
    }
}

export const loginSuccessful = (token, role) => {
    return {
        type: types.LOGIN_SUCCESSFUL,
        token, 
        role
    }
}

export const loginError = () => {
    return {
        type: types.LOGIN_ERROR
    }
}







