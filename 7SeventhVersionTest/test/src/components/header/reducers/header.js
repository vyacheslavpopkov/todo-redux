import * as types from '../actions/ActionTypes';

let initialState = {
    language: {
        leng: 'es-MX',
        id: 1
    }
};

function headerReducer(state = initialState, action) {
    switch (action.type) {

        case types.CHANGE_LANGUAGE:
            return updateObject(state, {
                language: {
                    leng: action.language,
                    id: action.id
                }
            });

        default:
            return state;
    }

    function updateObject(obj, newProperties) {
        return Object.assign({}, obj, newProperties);
    }
}

export default headerReducer;
