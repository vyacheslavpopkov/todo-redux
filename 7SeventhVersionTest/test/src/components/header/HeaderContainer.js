import React, { Component } from "react";
import { connect } from "react-redux";
import { NavLink, Route } from 'react-router-dom';
import { Button } from "react-bootstrap";
import { changeLanguage } from './actions/header';
import { getLocalization } from '../localization/middleware/localization';
import { getLocalText } from '../localization/LocalizationContainer';


class HeaderContainer extends React.Component {

    changeLanguageOnEsp = () => {
        this.props.changeLanguage('es-MX', 1);
    }

    changeLanguageOnEng = () => {
        this.props.changeLanguage('en-US', 0);
    }

    render() {
        return <nav>
            <ul>
                <li>
                    <NavLink to='/todos'>
                        {
                            getLocalText(getLocalization().localList[this.props.language.id].TODOS, this.props.language.leng).output
                        }
                    </NavLink>
                </li>
                <li>
                    <NavLink to='/about'>
                        {
                            getLocalText(getLocalization().localList[this.props.language.id].ABOUT, this.props.language.leng).output
                        }
                    </NavLink>
                </li>
            </ul>
            <Button onClick={this.changeLanguageOnEsp.bind(this)}> Esp </Button>
            <Button onClick={this.changeLanguageOnEng.bind(this)}> Eng </Button>
            <hr />
        </nav>
    }
}

const mapStateToProps = (state) => {
    return {
        language: state.headerReducer.language
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        changeLanguage: (language, id) => dispatch(changeLanguage(language, id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(HeaderContainer);



