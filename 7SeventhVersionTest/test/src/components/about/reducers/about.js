import * as types from '../actions/ActionTypes';

let initialState = {
    text: "About this website"
};

function aboutReducer(state = initialState, action) {
    switch (action.type) {
    

        default:
            return state;
    }

    function updateObject(obj, newProperties) {
        return Object.assign({}, obj, newProperties);
    }
}

export default aboutReducer;
