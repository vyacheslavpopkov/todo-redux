import React from 'react';
import { Provider } from 'react-redux';
import AboutContainer from '../AboutContainer';
import renderer from 'react-test-renderer';
import thunk from 'redux-thunk';
import aboutReducer from '../reducers/about';
import { applyMiddleware, combineReducers, createStore } from 'redux';

it('renders a snapshot', () => {
    
    let middlewares = [
        thunk
    ];
    
    let testReducers = combineReducers({
        aboutReducer
    });
    let store = createStore(testReducers, applyMiddleware(...middlewares));

    const tree = renderer.create(
        <Provider store={store}>
            <AboutContainer />
        </Provider>
    ).toJSON();
    expect(tree).toMatchSnapshot();
});