import * as types from '../actions/ActionTypes';
import { localizationWasSuccessful, localizationError } from '../actions/localization';
import { loadAllLocalRequest } from '../api/localization';

let globalLocalData = {}

function localizationMiddleware({ getState }) {
    return next => action => {
        switch (action.type) {

            case types.LOAD_LOCALIZATION:
            loadAllLocalRequest()
                    .then(response => {
                        if (response) {
                            globalLocalData = response;
                            next(localizationWasSuccessful())
                        }
                        else
                            next(localizationError())
                    })
                    .catch(e => console.error(e))
                break;

            default:
                break;
        }
        return next(action)
    }
}

export const getLocalization = () => {
    return Object.assign({}, globalLocalData);
}

export default localizationMiddleware;