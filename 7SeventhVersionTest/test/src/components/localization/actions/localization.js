
import * as types from './ActionTypes';

export const localizationWasSuccessful = () => {
    return {
        type: types.LOCALIZATION_WAS_SUCCESSFUL
    }
}

export const localizationError = () => {
    return {
        type: types.LOCALIZATION_ERROR
    }
}

