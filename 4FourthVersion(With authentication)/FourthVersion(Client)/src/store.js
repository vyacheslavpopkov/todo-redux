import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createLogger } from 'redux-logger'

import authorizationReducer from './components/authorization/reducers/authorization';
import toDoReducer from './components/toDo/reducers/toDo';
import toDosReducer from './components/toDos/reducers/toDos';


import authorizationMiddleware from './components/authorization/middleware/authorization';
import toDoMiddleware from './components/toDo/middleware/toDo';
import toDosMiddleware from './components/toDos/middleware/toDos'

const middleware = [
    thunk,
    authorizationMiddleware,
    toDoMiddleware,
    toDosMiddleware,
    createLogger(),
];


const store = createStore(
    combineReducers({
        authorizationReducer: authorizationReducer,
        toDoReducer: toDoReducer,
        toDosReducer: toDosReducer
        
    }),
    composeWithDevTools(applyMiddleware(...middleware))
);

export default store;


