import * as types from '../actions/ActionTypes';
import { toDosLoaded } from '../actions/toDos';
import { loadAllToDosRequest } from '../api/toDos';

function toDosMiddleware({ getState }) {
    return next => action => {
        switch (action.type) {
           
            case types.RELOAD_ALL_TODOS:
            case types.LOAD_TODOS:
                loadAllToDos(next)
                break;

            default:
                break;
        }
        return next(action)
    }
}

const loadAllToDos = (next) => {
    loadAllToDosRequest()
        .then(response => next(toDosLoaded(response)))
        .catch(e => console.error(e));
};


export default toDosMiddleware;