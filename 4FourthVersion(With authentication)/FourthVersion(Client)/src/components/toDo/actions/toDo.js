import * as types from './ActionTypes';

export const deleteToDo = (id) => {
    return {
        type: types.DELETE_TODO,
        id
    }
}

export const completeToDo = (id) => {
    return {
        type: types.COMPLETE_TODO,
        id
    }
}

export const editToDo = (toDo) => {
    return {
        type: types.EDIT_TODO,
        toDo
    }
}

export const reloadAllToDos = () => {
    return {
        type: types.RELOAD_ALL_TODOS
    }
}




