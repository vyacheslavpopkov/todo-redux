import axios from '../../globalApi';

const POST_ADD_TODO_URL = "/todo/todos";
const PUT_UPDATE_TODO_URL = "/todo/update";
const PUT_COMPLETE_TODO_URL = "/todo/complete/";
const DELETE_TODO_URL = "/todo/delete/";

export const postAddToDoRequest = (toDo) => {
    return axios.post(POST_ADD_TODO_URL, toDo)
        .then(response => {
            return response.data
        })
};

export const putUpdateToDoRequest = (toDo) => {
    return axios.put(PUT_UPDATE_TODO_URL, toDo)
        .then(response => {
            return response.data
        })
};

export const putCompleteToDoRequest = (id) => {
    return axios.put(PUT_COMPLETE_TODO_URL + id)
        .then(response => {
            return response.data
        })
};

export const deleteToDoRequest = (id) => {
    return axios.delete(DELETE_TODO_URL +  id)
        .then(response => {
            return response.data
        })
};



