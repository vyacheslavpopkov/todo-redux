import * as types from '../actions/ActionTypes';
import { reloadAllToDos } from '../actions/toDo';
import { postAddToDoRequest, putUpdateToDoRequest, putCompleteToDoRequest, deleteToDoRequest } from '../api/toDo';


function toDoMiddleware({ getState }) {
    return next => action => {
        switch (action.type) {

            case types.ADD_TODO:
                postAddToDoRequest(action.toDo)
                    .then(some => next(reloadAllToDos()))
                    .catch(e => console.error(e));
                break;

            case types.UPDATE_TODO:
                putUpdateToDoRequest(action.toDo)
                    .then(some => next(reloadAllToDos()))
                    .catch(e => console.error(e));
                break;

            case types.COMPLETE_TODO:
                putCompleteToDoRequest(action.id)
                    .then(some => next(reloadAllToDos()))
                    .catch(e => console.error(e));
                break;

            case types.DELETE_TODO:
                deleteToDoRequest(action.id)
                    .then(some => next(reloadAllToDos()))
                    .catch(e => console.error(e));
                break;

            default:
                break;
        }
        return next(action)
    }
}


export default toDoMiddleware;