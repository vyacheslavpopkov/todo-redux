import axios from '../../globalApi';

const POST_LOGIN_URL = "/account/token/";

let authProxy = 0;

export const logInRequest = (user) => {
    return axios.post(POST_LOGIN_URL, user)
        .then(response => {
            authProxy = axios.interceptors.request.use(function (config) {
                config.headers = {
                    'Authorization': 'Bearer ' + response.data.access_token
                };

                return config;
            }, function (error) {
                // Do something with request error
                return Promise.reject(error);
            });

            return response.data
        })
};

export const logOutRequest = () => {
    return axios.interceptors.request.eject(authProxy);
};






