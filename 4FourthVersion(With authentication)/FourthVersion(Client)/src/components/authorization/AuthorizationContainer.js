import React, { Component } from "react";
import { connect } from "react-redux";
import { FormControl, Button, Grid } from "react-bootstrap";
import { updateMail, updatePassword, logIn } from './actions/authorization';
import ToDosContainer from '../toDos/ToDosContainer';
import './css/authorization.css';

class AuthorizationContainer extends Component {
 
    updateMail = (e) => {
        this.props.updateMail(e.target.value);
    }

    updatePassword = (e) => {
        this.props.updatePassword(e.target.value);
    }

    logIn = () => {
        let user = {
            login: this.props.mail,
            password: this.props.password
        }
        this.props.logIn(user);
    }

    render() {
        return (
            (this.props.isInside)
            ?<ToDosContainer/>
            :<Grid>
                <FormControl
                    value={this.props.mail}
                    onChange={this.updateMail.bind(this)} />
                <FormControl
                    value={this.props.password}
                    onChange={this.updatePassword.bind(this)} />

                <Button onClick={this.logIn.bind(this)}>logIn</Button>
                {
                    (this.props.error.isError)
                    ? <div id="error"> {this.props.error.errorText} </div>
                    : <div> </div>
                }
            </Grid>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        mail: state.authorizationReducer.mail,
        password: state.authorizationReducer.password,
        isInside: state.authorizationReducer.isInside,
        error: state.authorizationReducer.error
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateMail: (mail) => dispatch(updateMail(mail)),
        updatePassword: (password) => dispatch(updatePassword(password)),
        logIn: (user) => dispatch(logIn(user))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AuthorizationContainer);
