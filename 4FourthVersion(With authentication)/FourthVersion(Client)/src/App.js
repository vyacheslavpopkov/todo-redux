import React, { Component } from 'react';
import AuthorizationContainer from './components/authorization/AuthorizationContainer'


class App extends Component {

  render() {
    return (
      <div>
      <AuthorizationContainer/>
      </div>
    );
  }
}

export default App;
